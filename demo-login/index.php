<?php 
require_once "conf/Config.php";
require_once ROOT_PATH ."/dao/UsuarioDAO.php";
require_once ROOT_PATH ."/model/Usuario.php";
require_once ROOT_PATH ."/controller/SessionController.php";

$controller = new SessionController();

if(!$controller->estaAutenticado()) {
    header("Location: login.php");
    die();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registro de Usuarios</title>
        
        <script>
        
            function modificar(usuario) {
                location.href='editar.php?username='+usuario;
            }
        
            function eliminar(usuario) {
                document.formularioEliminar.username.value=usuario;
                formularioEliminar.submit();
            }

        </script>
        
    </head>
    <body>
        <h1>Demo Admin Login</h1>

        [<a href="logout.php" >Logout <?= $controller->getNombreUsuarioAutenticado() ?></a>]
        
<?php 
    $usuarios = $controller->getUsuarios();
?>
        <table>
            <thead>
                <tr>
                    <th>USERNAME</th>
                    <th>NOMBRE</th>
                    <th>APELLIDO</th>
                    <th>FECHA_NACIMIENTO</th>
                    <th>MODIFICAR</th>
                    <th>ELIMINAR</th>
                </tr>
            </thead>
            <tbody>
                
                <?php
                    foreach($usuarios as $user) {
                        /* @var $user Usuario */
                ?>

                <tr>
                    <td><?= $user->getUsername() ?></td>
                    <td><?= $user->getNombre() ?></td>
                    <td><?= $user->getApellido() ?></td>
                    <td><?= $user->getFechaNacimiento() ?></td>
                    <td>
                        <input type="button" 
                               name="modificar" 
                               value="Modificar" 
                               onclick="javascript:modificar('<?= $user->getUsername() ?>');" />
                    </td>
                    <td>
                        <input type="button" 
                               name="eliminar" 
                               value="Eliminar" 
                               onclick="javascript:eliminar('<?= $user->getUsername() ?>');" />
                    </td>
                </tr>
                <?php
                    }
                ?>                
            </tbody>            
        </table>
        
        <form name="formularioEliminar" id="formularioEliminar" 
              action="eliminar.php" method="POST">
            <input type="hidden" name="username" value=""/>
        </form>
    </body>
</html>
