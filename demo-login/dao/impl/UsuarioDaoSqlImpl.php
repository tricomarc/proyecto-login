<?php

require_once ROOT_PATH ."/dao/UsuarioDAO.php";
require_once ROOT_PATH ."/model/Usuario.php";

class UsuarioDaoSqlImpl extends PDO implements UsuarioDAO  {
    
    public function agregar($objeto) {
        $username = $objeto->getUsername();
        $nombre = $objeto->getNombre();
        $apellido = $objeto->getApellido();
        $fecha_nacimiento = $objeto->getFechaNacimiento();
        $pass = $objeto->getPassword();
        
        $pdoStatement = $this->prepare("INSERT INTO usuario (username, nombre, apellido, feche_nacimiento, password)
                                        VALUES (:username, :nombre, :apellido, :fecha_nacimiento, :pass");
        $pdoStatement->bindParam(':username', $username);
        $pdoStatement->bindParam(':nombre', $nombre);
        $pdoStatement->bindParam(':apellido',$apellido);
        $pdoStatement->bindParam(':fecha_nacimiento', $fecha_nacimiento);
        $pdoStatement->bindParam(':pass', $pass);
        $pdoStatement->execute();
    }

    public function eliminar($objeto) {
        
    }

    public function getByUsername($username) {
        $pdoStatement = $this->prepare("SELECT username, nombre, apellido, fecha_nacimiento, password FROM usuario WHERE username = ?");
        $pdoStatement->bindParam(1, $username);
        $pdoStatement->execute();
        
        $row = $pdoStatement->fetch();
        $usuario = new Usuario();
        $usuario->setUsername($row["username"]);
        $usuario->setNombre($row["nombre"]);
        $usuario->setApellido($row["apellido"]);
        $usuario->setFechaNacimiento($row["fecha_nacimiento"]);
        $usuario->setPassword($row["password"]);
        
        return $usuario;        
    }

    public function listar() {
        $usuarios = Array();
        
        $pdoStatement = $this->prepare("SELECT username, nombre, apellido, fecha_nacimiento FROM usuario");
        $pdoStatement->execute();
        
        while($row = $pdoStatement->fetch()) {
            $usuario = new Usuario();
            $usuario->setUsername($row["username"]);
            $usuario->setNombre($row["nombre"]);
            $usuario->setApellido($row["apellido"]);
            $usuario->setFechaNacimiento($row["fecha_nacimiento"]);
            
            array_push($usuarios, $usuario);
        }
        
        return $usuarios;
    }

    public function modificar($objeto) {
        
    }
    
    public function __construct($file = SETTINGS_FILE)
    {
        if (!$settings = parse_ini_file($file, TRUE)) {
            throw new exception('No se pudo abrir el archivo ' . $file . '.');
        }
       "mysql:localhost;port=3306;dbname=dai5501";
       
       $driver = $settings['database']['driver'];
       $host = $settings['database']['host'] ;
       $port = $settings['database']['port'];
       $dbname = $settings['database']['dbname'];
       $user = $settings['database']['username'];
       $password = $settings['database']['password'];
       
       $dns =  $driver. ":host=".$host.";port=".$port.";dbname=".$dbname;
               
        parent::__construct($dns,$user, $password);
    }
}
