<?php

require_once ROOT_PATH ."/dao/UsuarioDAO.php";
require_once ROOT_PATH ."/dao/impl/UsuarioDaoSqlImpl.php";

class SessionController {
    
    /**
     *
     * @var UsuarioDAO 
     */
    private $daoUsuario;
    
    public function __construct() {
        $this->daoUsuario = new UsuarioDaoSqlImpl();
    }
    
    function getUsuarios() {
        $usuarios = $this->daoUsuario->listar();
        
        return $usuarios;        
    }
    
    function estaAutenticado() {
        
        if(isset($_SESSION["username"]) && $_SESSION["username"]!= "") {
            return true;
        }
        
        return false;
    }
    
    function estaEnviandoCredenciales() {
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST["username"])) {
            return true;
        }
        
        return false;
    }
    
    function autenticarUsuario($usuario, $clave) {
        
        $autenticado = false;
        
        /* @var $user Usuario */
        $user = $this->daoUsuario->getByUsername($usuario);
        
        if(isset($user) && $user->isCredentialValid($clave)) {
            $_SESSION["username"] = $user->getUsername();
            $autenticado = true;
        } 
        
        return $autenticado;
    }
    
    function getNombreUsuarioAutenticado() { 
        return $_SESSION["username"];
    }
    
    function logout() { 
        unset($_SESSION["username"]);
        session_destroy();          
    }
}
