<?php

class Usuario {
    private $username;
    private $nombre;
    private $apellido;
    private $fechaNacimiento;
    private $password;
    
    function getUsername() {
        return $this->username;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function isCredentialValid($password) {
        return $this->password == $password;
    }

}
