CREATE TABLE usuario (
	username VARCHAR(16) NOT NULL PRIMARY KEY,
	nombre VARCHAR(32) NOT NULL,
	apellido VARCHAR(32) NOT NULL,
	fecha_nacimiento DATE NOT NULL,
	password VARCHAR(128) NOT NULL
);

